import { Injectable } from '@angular/core';
declare var $;

/**
 * Class for managing stylesheets. Stylesheets are loaded into named slots so that they can be
 * removed or changed later.
 */
@Injectable()
export class StyleManagerService {



  startvalue(nbreData:number){
    
    var fiveStars = starsOutput('star','star','star','star','star');
    var fourHalfStars = starsOutput('star','star','star','star','star half');
    var fourStars = starsOutput('star','star','star','star','star empty');
    var threeHalfStars = starsOutput('star','star','star','star half','star empty');
    var threeStars = starsOutput('star','star','star','star empty','star empty');
    var twoHalfStars = starsOutput('star','star','star half','star empty','star empty');
    var twoStars = starsOutput('star','star','star empty','star empty','star empty');
    var oneHalfStar = starsOutput('star','star half','star empty','star empty','star empty');
    var oneStar = starsOutput('star','star empty','star empty','star empty','star empty');


          // Rules
          if (nbreData >= 4.75) {
            return fiveStars;

        } else if (nbreData >= 4.25) {
          return fourHalfStars;
        } else if (nbreData >= 3.75) {
          return fourStars;
        } else if (nbreData >= 3.25) {
          return threeHalfStars;
        } else if (nbreData >= 2.75) {
          return threeStars;
        } else if (nbreData >= 2.25) {
          return twoHalfStars;
        } else if (nbreData >= 1.75) {
          return twoStars;
        } else if (nbreData >= 1.25) {
          return oneHalfStar;
        } else if (nbreData < 1.25) {
          return oneStar;
        }
  }





  /**
   * Set the stylesheet with the specified key.
   */
  setStyle(key: string, href: string) {
    getLinkElementForKey(key).setAttribute('href', href);
  }

  /**
   * Remove the stylesheet with the specified key.
   */
  removeStyle(key: string) {
    const existingLinkElement = getExistingLinkElementByKey(key);
    if (existingLinkElement) {
      document.head.removeChild(existingLinkElement);
    }
  }

}

function getLinkElementForKey(key: string) {
  return getExistingLinkElementByKey(key) || createLinkElementWithKey(key);
}

function getExistingLinkElementByKey(key: string) {
  return document.head.querySelector(`link[rel="stylesheet"].${getClassNameForKey(key)}`);
}

function createLinkElementWithKey(key: string) {
  const linkEl = document.createElement('link');
  linkEl.setAttribute('rel', 'stylesheet');
  linkEl.classList.add(getClassNameForKey(key));
  document.head.appendChild(linkEl);
  return linkEl;
}

function getClassNameForKey(key: string) {
  return `style-manager-${key}`;
}

  // Rating Stars Output
 function starsOutput(firstStar:string, secondStar:string, thirdStar:string, fourthStar:string, fifthStar:string) {

    let valueData =  ''+
    '<span class="'+firstStar+'"></span>'+
    '<span class="'+secondStar+'"></span>'+
    '<span class="'+thirdStar+'"></span>'+
    '<span class="'+fourthStar+'"></span>'+
    '<span class="'+fifthStar+'"></span>';
  
  return valueData;
  }




