import { Component, OnInit,EventEmitter, Output,Input  } from '@angular/core';

@Component({
  selector: 'app-filtering-section',
  templateUrl: './filtering-section.component.html',
  styleUrls: ['./filtering-section.component.css']
})
export class FilteringSectionComponent implements OnInit {

  @Input() displayMode:string ='list';
  @Output() changeMode: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  OnchangeMode(mode:string){

    this.displayMode = mode;
    this.changeMode.emit(this.displayMode);

  }


}
