import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-search',
  templateUrl: './form-search.component.html',
  styleUrls: ['./form-search.component.css']
})
export class FormSearchComponent implements OnInit {

  categories :{title:string, value:string } []= [
    {
    title:'Bodyguard',
    value:'bodyguard' 
  },
  {
    title:'Bras droit',
    value:'bras-droit' 
  },
  { 
    title:'Autre',
    value:'other' 
  }

  ];
  constructor() { }

  ngOnInit(): void {
  }


  
  trackByFn(index, item) {
    return index;
  }
}
