import { Component, OnInit,HostListener,AfterContentInit,OnDestroy } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { Lightbox } from 'ngx-lightbox';
import { StyleManagerService } from "src/app/core/services/style-manager.service";
declare var $;

@Component({
  selector: 'app-freelance-page',
  templateUrl: './freelance-page.component.html',
  styleUrls: ['./freelance-page.component.css']
})
export class FreelancePageComponent implements OnInit,OnDestroy,AfterContentInit {
  

  slideConfig = {
    "centerMode": true,
		"centerPadding": '20%',
		"slidesToShow": 2,
		"responsive": [
			{
			  "breakpoint": 1367,
        "settings": {
			    "centerPadding": '15%'
			  }
			},
			{
			  "breakpoint": 1025,
			  "settings": {
			    "centerPadding": '0'
			  }
			},
			{
			  "breakpoint": 767,
			  "settings": {
			    "centerPadding": '0',
			    "slidesToShow": 1
			  }
			}
		]
  };
  banner_slider_images:any = [];


   title:string="Albert Kouassi";
   location:string="Cocody Phamarcie Mermoz, Abidjan, Cote D ivoire";
   avis:number= 12;
   nbreRating:number =3.5;
   categorie:string="Bras-droit";

  constructor(private _lightbox: Lightbox,
              private styleCustom: StyleManagerService,
              private viewportScroller: ViewportScroller) { 

    for (let i = 1; i <= 4; i++) {
      const src = 'assets/images/single-listing-0' + i + '.jpg';
      const caption = 'Image ' + i + ' caption here';
      const thumb = 'assets/images/single-listing-0' + i + '.jpg';
      const item = {
         src: src,
         caption: caption,
         thumb: thumb
      };

      this.banner_slider_images.push(item);
    }
  }

  ngOnInit(): void {
    //this.styleCustom.starRating('.star-rating');
  }

  open(index: number): void {
    // open lightbox
    this._lightbox.open(this.banner_slider_images, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  ngAfterContentInit (){
    
    this.onLoad();

    
  }
 
  onClickScroll(elementId: string): void { 
    this.viewportScroller.scrollToAnchor(elementId);
}

  /* scroll effet for listings nav */

      @HostListener("window:scroll", ["$event"])
      onWindowScroll($event) {
           if(document.getElementById("listing-nav") !== null) { 
                var window_top = document.documentElement.scrollTop;

                var div_top = 0;
                if($('.listing-nav').length){
                  div_top = $('.listing-nav').not('.listing-nav-container.cloned .listing-nav').offset().top + 90;
                }
                                              

                if (window_top > div_top) {
                  $('.listing-nav-container.cloned').addClass('stick');
              } else {
                  $('.listing-nav-container.cloned').removeClass('stick');
              }
          }
    }

   

 /* load sticky listing and other */
      onLoad() {
       

                  //add clone for listing

                   $( ".listing-nav-container" ).clone(true).addClass('cloned').prependTo("body");

                  // add style for clone nav listing for sticky

                    var containerWidth = $(".container").width();
                    var containerHeight = $(".container").height();
                    containerHeight = containerHeight + 17;
                      $('.listing-nav-container.cloned .listing-nav').css('width', containerWidth);
                      $('.listing-nav-container.cloned .listing-nav').css('height', containerHeight);


                  /*  for Highlighting functionality */

                    var aChildren = $(".listing-nav li").children();
                    var aArray = [];
                    for (var i=0; i < aChildren.length; i++) {
                        var aChild = aChildren[i];
                        var ahref = $(aChild).attr('href');
                        aArray.push(ahref);
                    }

                if(document.getElementById("listing-nav") !== null){
                  $(window).scroll(function(){
                   // var windowPos = $(window).scrollTop();
                   let scrollToTopValue = document.documentElement.scrollTop;
                    for (var i=0; i < aArray.length; i++) {
                      var theID = aArray[i];
                      var divPos = 0;
                      var divHeight = 0;

                      if($(theID).length){
                         divPos = $(theID).offset().top - 150;
                         divHeight = $(theID).height();
                      }

                      if (scrollToTopValue >= divPos && scrollToTopValue < (divPos + divHeight)) {
                        $("a[href='" + theID + "']").addClass("active");
                      } else {
                        $("a[href='" + theID + "']").removeClass("active");
                      }
                    }
                  });
                }
      }

     
      ngOnDestroy(){
        $( ".listing-nav-container" ).remove();
      }
}
